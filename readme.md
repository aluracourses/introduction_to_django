# Practicing Django

## Project vs app

- Project == collection of settings
- app == application that does something


## Dependencies

```sh
# Create environment
...
```

```sh
pip3 install -r requirements.txt
```

-  Python
-  Django
-  Docker
-  MySQL
-  Html + JS + CSS

## Init Project

```sh
# init project
django-admin.py startproject $project_name .


# init bd
python3 ./manage.py migrate
```

## Create new app

```sh
# create app
python3 manage.py startapp $app_name

# create user
python3 manage.py createsuperuser
```

```sh
# make django copy static files into the applications
python3 manage.py collectstatic

# models to banco de dados
python3 manage.py makemigrations
python3 manage.py migrate

```


## How to run

```sh
start docker
docker-compose up -d

# run server
python3 manage.py runserver 0.0.0.0:$port
```

## About the project:

- Alura recipients
    - TODO: SS

- CRUD of recipients
- Introduction to Django
- ORM + Django
- Django migrations
