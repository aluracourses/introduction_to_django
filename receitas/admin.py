
from django.contrib import admin

from .models import Receita

class ListRecipients(admin.ModelAdmin):
    # set how to diplay the recipients to the admin
    list_display = ('id', 'name', 'categoria', 'active')

    # set element linkable
    list_display_links = ('id', 'name', 'categoria',)

    # add filters to searchable
    search_fields = ('name', 'categoria')

    # pagination
    list_per_page = 10

    # editable directly from home
    list_editable = ('active', )


admin.site.register(Receita, ListRecipients)
