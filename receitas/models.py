import django
from django.db import models
# from django.utils.timezone import now
# from timezone.timezone import now
from peoples.models import Peoples

class Receita(models.Model):

    people = models.ForeignKey(Peoples, on_delete=models.CASCADE)

    name = models.CharField(max_length=200)
    ingredientes = models.TextField()
    modo_preparo = models.TextField()
    tempo_preparo = models.IntegerField()
    rendimento = models.CharField(max_length=100)
    categoria = models.CharField(max_length=100)
    # data_receita = models.DateTimeField(default=now(), blank=True)
    data_receita = models.DateTimeField(
        default=django.utils.timezone.now, blank=True)
    active = models.BooleanField(default=True)
    photo_receita = models.ImageField(upload_to='fotos/%Y-%m-%d/', blank=True)
