"""
docstring
"""
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render

from receitas.models import Receita


def index(req: HttpResponse):
    """
    docstring
    """
    receitas = Receita.objects.filter(active=True).order_by("-data_receita")

    context = {
        'receitas': receitas
    }


    return render(req, 'index.html', context=context)

def receita(req:HttpResponse, receita_id:int):
    """
    docstring
    """
    receita_from_db = get_object_or_404(Receita, pk=receita_id, active=True)

    context = {
        'receita': receita_from_db
    }
    return render(req, 'receita.html', context=context)
