from django.contrib import admin

from peoples.models import Peoples


# Register your models here.
class ListPeoples(admin.ModelAdmin):
    # set how to diplay the recipients to the admin
    list_display = ('id', 'name', 'email')

    # set element linkable
    list_display_links = list_display

    # add filters to searchable
    search_fields = list_display

    # pagination
    list_per_page = 10


admin.site.register(Peoples, ListPeoples)
