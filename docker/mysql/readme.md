# Para que serve essa imagem
[...]

## Construir imagem

./build_images-linux.sh


## Criar a imagem

./create_container-linux.sh

<!-- ## Copiar sql do servidor

```sh


mysql -u username -p -h remote.site.com

mysqldump -u username -p -h remote.site.com DBNAME > backup.sql

``` -->

## Entrar no container

```sh
sudo docker exec -it $container_name bash

# Create user
CREATE USER 'newuser' IDENTIFIED BY 'password';

GRANT ALL PRIVILEGES ON *.* TO 'newuser'@'*';

FLUSH PRIVILEGES;
```




executar mysql

```sh
mysql -u username -p $database_name < /sql/<file.sql>
```
